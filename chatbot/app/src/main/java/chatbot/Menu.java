/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package chatbot;

import java.util.*;
class Menu{
  public static void main(String[] args) throws Exception {
    Scanner sc = new Scanner(System.in);
    int userRequest = 0;

    Node begin = new Node();
    Node level1 = new Node();
    Node level2 = new Node();
    Node level3 = new Node();
    Node level4 = new Node();
    Node level5 = new Node();
    begin.setChild(level1);
    begin.setAction(new ActionName());
    level1.setAction(new ActionWeather());
    level1.setChild(level2);
    level2.setAction(new ActionVopros());
    level2.setChild(level3);
    level3.setAction(new ActionChoise());
    level3.setChild(level4);
    level4.setAction(new Music());
    level3.setChild(level5);
    level5.setAction(new Books());

    System.out.println("Добро пожаловать в чат-бот!\nЗдесь вы можете найти подборку музыки и книг!");
    ActionName one = new ActionName("Добро пожаловать в чат-бот!\nЗдесь вы можете найти подборку музыки и книг!");
    one.name();

    System.out.println("Какая у вас погода?");
    ActionWeather two = new ActionWeather("Какая у вас погода?");
    two.weather();

    System.out.println("Хотите ли вы подобрать топ музыки или песен?");
    Action three = new Action("Хотите ли вы подобрать топ музыки или песен?");
    three.setMessage("1. да\n2. нет");
    String userTop = sc.nextLine();

    System.out.println("1. Музыка или 2. книги?");
    Action four = new Action("Музыка или книги?");
    four.setMessage("1.Музыка \n2. Книги");
    int userrequest = sc.nextInt();

    if (userRequest == 1){
      Music five = new Music("Какой жанр музыки вы препочитаете?");
      five.setActmusic();
    }
    else{
      Books six = new Books("Какой жанр музыки вы препочитаете?");
      six.setActbook();
    }

    System.out.println("Ждем вас еще! Чтобы выйти, нажмите 0");

    while (true){
      // begin.output();
      userRequest = sc.nextInt();
      System.out.println();
      if (userRequest == 0){
        if (begin.getParent() == null){
          break;
        }
        else{
          begin = begin.getParent();
        }
      }
      if (userRequest > begin.sizeChild() && userRequest <= begin.sizeAction() + begin.sizeChild()){
        begin.action.get(userRequest - begin.sizeChild() - 1);
      }
      if (userRequest > 0 && userRequest <= begin.sizeChild()){
        begin = begin.child.get(userRequest - 1);
      }
    }

  }
}
